const fs = require('fs');
let content = '';

fs.readFile('input.txt', function read(err, data) {
    if (err) {
        throw err;
    }
    content = data;
    processFile();
});

function processFile() {
    const values = content.toString().split("\n");

    console.log('--- Day 1: Chronal Calibration ---');
    console.log('Part One Answer: ', partOne(values));
    console.log('Part Two Answer: ', partTwo(values));
}

function partOne(values) {
    let totalFrequency = 0;

    values.forEach((entry) => {
        totalFrequency += Number(entry);
    });

    return totalFrequency;
}

function partTwo(values) {
    let frequency = 0;
    let currentPos = 0;
    let prevousFrequencies = [];

    while(true) {
        frequency += Number(values[currentPos]);

        if(prevousFrequencies.indexOf(frequency) !== -1) {
            return frequency;
        } else {
            prevousFrequencies.push(frequency);
        }

        currentPos = (currentPos + 1) % (values.length - 1);
    }
}
